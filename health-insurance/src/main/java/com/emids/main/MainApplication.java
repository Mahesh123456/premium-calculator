package com.emids.main;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import com.emids.model.CurrentHealth;
import com.emids.model.Habit;
import com.emids.model.Person;
import com.emids.util.PremiumCalculator;

public class MainApplication {
	public static void main(String[] args) {

		PremiumCalculator p = new PremiumCalculator();
		Person person = new Person();
		List<CurrentHealth> currentHealthList = new ArrayList<CurrentHealth>();
		List<Habit> habitList = new ArrayList<Habit>();
		currentHealthList.add(new CurrentHealth("Overweight", true));
		currentHealthList.add(new CurrentHealth("Blood sugar", false));
		currentHealthList.add(new CurrentHealth("Blood pressure", false));
		currentHealthList.add(new CurrentHealth("Hypertension", false));
		Habit h = null;
		h = new Habit("Smoking", false, false);
		habitList.add(h);
		h = new Habit("Alcohol", true, false);
		habitList.add(h);
		h = new Habit("Daily exercise", true, true);
		habitList.add(h);
		h = new Habit("Drugs", false, false);
		habitList.add(h);

		person.setAge(34);
		person.setName("Norman Gomes");
		person.setGender("Male");
		person.setCurrentHealth(currentHealthList);
		person.setHabits(habitList);
		System.out.println("Health Insurance Premium for "+person.getName()+" :Rs " + NumberFormat.getInstance().format(p.getPremium(person)));
	}
}
