package com.emids.util;

import java.util.List;

import com.emids.model.CurrentHealth;
import com.emids.model.Habit;
import com.emids.model.Person;

public class PremiumCalculator {

	private int basePremium = 5000;

	private int getPremiumBasedOnAge(Integer age) {
		int count;
		int firstBreakDown = (basePremium + (int) (basePremium * 0.1));
		int secondBreakDown = firstBreakDown + (int) (firstBreakDown * 0.1);
		int thirdBreakDown = secondBreakDown + (int) (secondBreakDown * 0.1);
		int fourthBreakDown = thirdBreakDown + (int) (thirdBreakDown * 0.1);

		if (age >= 18 && age < 25) {
			return firstBreakDown;
		}

		else if (age >= 25 && age < 30) {
			return secondBreakDown;
		}

		else if (age >= 30 && age < 35) {
			return thirdBreakDown;
		}

		else if (age >= 35 && age < 40) {
			return fourthBreakDown;
		}

		else if (age > 40) {
			int fifthBreakDown = fourthBreakDown + (int) (fourthBreakDown * 0.05);
			count = (age / 5) - 7;
			while (count >= 0) {
				fifthBreakDown += (int) (fifthBreakDown * 0.05);
				count--;
			}
			return fifthBreakDown;
		}
		return basePremium;
	}

	public int getPremium(Person p) {
		int personPremium = 0;
		int goodHabit = 0;
		int badHabit = 0;
		personPremium = getPremiumBasedOnAge(p.getAge());

		if ("male".equalsIgnoreCase(p.getGender())) {
			personPremium = personPremium + (int) Math.ceil(personPremium * 0.02);

		}
		List<CurrentHealth> currentHealth = p.getCurrentHealth();
		List<Habit> habits = p.getHabits();
		for (CurrentHealth c : currentHealth) {
			if (c.isTrue()) {
				personPremium = personPremium + (int) (personPremium * 0.01);
			}
		}
		for (Habit h : habits) {
			if (h.isTrue()) {
				if (h.isGood()) {
					goodHabit += (int) (personPremium * 0.03);

				} else if (!h.isGood()) {
					badHabit += (int) (personPremium * 0.03);
				}
			}

		}
		return personPremium - goodHabit + badHabit;
	}

}
