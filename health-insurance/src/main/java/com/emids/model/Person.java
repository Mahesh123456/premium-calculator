package com.emids.model;

import java.io.Serializable;
import java.util.List;

public class Person implements Serializable {

	private static final long serialVersionUID = 1L;
	private String name;
	private String gender;
	private Integer age;
	private List<CurrentHealth> currentHealth;
	private List<Habit> habits;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public List<CurrentHealth> getCurrentHealth() {
		return currentHealth;
	}

	public void setCurrentHealth(List<CurrentHealth> currentHealth) {
		this.currentHealth = currentHealth;
	}

	public List<Habit> getHabits() {
		return habits;
	}

	public void setHabits(List<Habit> habits) {
		this.habits = habits;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		int prime = 3;
		return super.hashCode() * prime;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", gender=" + gender + ", age=" + age + ", currentHealth=" + currentHealth
				+ ", habits=" + habits + "]";
	}

}
