package com.emids.model;

import java.io.Serializable;

public class Habit implements Serializable{

	private static final long serialVersionUID = 1L;
	private String name;
	boolean isTrue;
	boolean isGood;
	
	public Habit(String name, boolean isTrue, boolean isGood) {
		super();
		this.name = name;
		this.isTrue = isTrue;
		this.isGood = isGood;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isTrue() {
		return isTrue;
	}
	public void setTrue(boolean isTrue) {
		this.isTrue = isTrue;
	}
	public boolean isGood() {
		return isGood;
	}
	public void setGood(boolean isGood) {
		this.isGood = isGood;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public String toString() {
		return "Habit [name=" + name + ", isTrue=" + isTrue + ", isGood=" + isGood + "]";
	}
	

}
