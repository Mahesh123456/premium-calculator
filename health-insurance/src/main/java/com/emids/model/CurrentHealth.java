package com.emids.model;

import java.io.Serializable;

public class CurrentHealth implements Serializable {

	private static final long serialVersionUID = 1L;
	private String name;
	boolean isTrue;

	public CurrentHealth(String name, boolean isTrue) {
		super();
		this.name = name;
		this.isTrue = isTrue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isTrue() {
		return isTrue;
	}

	public void setTrue(boolean isTrue) {
		this.isTrue = isTrue;
	}

	@Override
	public int hashCode() {
		int prime = 3;
		return super.hashCode() * prime;
	}

	@Override
	public String toString() {
		return "CurrentHealth [name=" + name + ", isTrue=" + isTrue + "]";
	}

}
